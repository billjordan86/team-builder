//
//  TBListItem.h
//  TeamBuilder
//
//  Created by PGM on 2/12/15.
//  Copyright (c) 2015 PGM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TBListItem : NSObject

//string used to get image from assets
@property NSString *imageStr;

@property NSString *name;

@property NSString *desc;

@end
