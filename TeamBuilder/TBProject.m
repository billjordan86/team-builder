//
//  TBProject.m
//  TeamBuilder
//
//  Created by PGM on 2/12/15.
//  Copyright (c) 2015 PGM. All rights reserved.
//

#import "TBProject.h"

@implementation TBProject

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setImageStr:@"defaultProjectPic"];
        [self setName:@"Project Name"];
        [self setDesc:@"This is a short message about the project"];
    }
    return self;
}

- (instancetype)initWithImageStr:(NSString*) imageStr Name:(NSString*) name Desc:(NSString*) desc {
    self = [super init];
    if (self) {
        [self setImageStr:imageStr];
        [self setName:name];
        [self setDesc:desc];
    }
    return self;
}

@end
