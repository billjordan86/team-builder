//
//  TBPerson.m
//  TeamBuilder
//
//  Created by PGM on 2/12/15.
//  Copyright (c) 2015 PGM. All rights reserved.
//

#import "TBPerson.h"

@implementation TBPerson

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setImageStr:@"defaultUserPic"];
        [self setName:@"Bill Jordan"];
        [self setDesc:@"This is a short message that the user wants people to see"];
    }
    return self;
}

- (instancetype)initWithImageStr:(NSString*) imageStr Name:(NSString*) name Desc:(NSString*) desc {
    self = [super init];
    if (self) {
        [self setImageStr:imageStr];
        [self setName:name];
        [self setDesc:desc];
    }
    return self;
}

@end
