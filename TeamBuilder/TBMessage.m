//
//  TBMessage.m
//  TeamBuilder
//
//  Created by PGM on 2/12/15.
//  Copyright (c) 2015 PGM. All rights reserved.
//

#import "TBMessage.h"

@implementation TBMessage

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setImageStr:@"defaultUserPic"];
        [self setName:@"Sam Brogan"];
        [self setDesc:@"This is a message from Sam Brogan"];
    }
    return self;
}

- (instancetype)initWithImageStr:(NSString*) imageStr Name:(NSString*) name Desc:(NSString*) desc {
    self = [super init];
    if (self) {
        [self setImageStr:imageStr];
        [self setName:name];
        [self setDesc:desc];
    }
    return self;
}

@end
